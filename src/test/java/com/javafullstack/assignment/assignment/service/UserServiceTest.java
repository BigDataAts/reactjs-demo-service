package com.javafullstack.assignment.assignment.service;


import com.javafullstack.assignment.assignment.entity.User;
import com.javafullstack.assignment.assignment.repo.UserRepo;

import com.javafullstack.assignment.assignment.service.impl.UserServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class UserServiceTest {

    @InjectMocks
    UserServiceImpl userService;

    @Mock
    private UserRepo userRepo;


    private  List<User> usersList = new ArrayList<>();
    @Before
    public  void init() {
        List<User> users = new ArrayList<>();
        users.add(new User(1,"James","James@123.com","1!23#4","EMPLOYEE"));
        users.add(new User(2,"Peter","Peter @123.com","8^23!3","EMPLOYEE"));
        users.add(new User(3,"John","John @123.com","98!891","ADMIN"));
        users.add(new User(4,"Fred","Fred @123.com","68651","ADMIN"));

        usersList =  users.stream().sorted(Comparator.comparing(User::getName)).collect(Collectors.toList());
    }


    @Test
    public void getAllUsersTest(){
        when(userRepo.findAll()).thenReturn(usersList);
        List<User> userList = userService.getUsers();
        Optional<User> firstUser =  userList.stream().findFirst();
        Optional<User> firstMockUser =  usersList.stream().findFirst();
        assertEquals(firstUser.isPresent()?firstUser.get().getName():"", firstMockUser.isPresent()?firstMockUser.get().getName():"");
    }

}
