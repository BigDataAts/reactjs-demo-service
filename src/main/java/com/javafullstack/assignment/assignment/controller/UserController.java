package com.javafullstack.assignment.assignment.controller;

import com.javafullstack.assignment.assignment.entity.User;
import com.javafullstack.assignment.assignment.exceptions.UserNotFountException;
import com.javafullstack.assignment.assignment.model.UserModel;
import com.javafullstack.assignment.assignment.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.awt.*;
import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin
@RequestMapping("/demo/v1")
public class UserController {

    @Autowired
    UserService userService;

    @GetMapping(value = "/users",produces = MediaType.APPLICATION_JSON_VALUE)
    public List<?> getUsers(){
        return userService.getUsers();
    }

    @RequestMapping(method = RequestMethod.GET,value = "/user/{userid}")
    public ResponseEntity<Object> getUser(@PathVariable("userid") Integer userid){
       Optional<User> user =  userService.findUserById(userid);
        if(user.isPresent())
            return new ResponseEntity<>(user.get(), HttpStatus.OK);
        else
            throw  new UserNotFountException("User not found....");
    }

    @RequestMapping(method = RequestMethod.POST,value = "/user")
    public ResponseEntity<Object> getUser(@RequestBody User userObj){
        Optional<User> user =  userService.findUserByEmail(userObj);
        if(user.isPresent())
            return new ResponseEntity<>(user.get(), HttpStatus.OK);
        else
            throw  new UserNotFountException("User not found....");
    }

    @RequestMapping(method = RequestMethod.POST,value = "/roleUsers")
    public List<UserModel> roleUsers(@RequestBody User userObj){
        return userService.getUsersByRole(userObj);
    }

}
