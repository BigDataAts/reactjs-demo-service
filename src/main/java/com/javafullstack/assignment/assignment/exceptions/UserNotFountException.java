package com.javafullstack.assignment.assignment.exceptions;

public class UserNotFountException extends RuntimeException {

    public UserNotFountException(String message){
            super(message);
    }

}
