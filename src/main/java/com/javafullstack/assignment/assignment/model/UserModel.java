package com.javafullstack.assignment.assignment.model;

import lombok.Data;

import javax.persistence.Column;

@Data
public class UserModel {

    private String name;

    private String email;

    private String role;
}
