package com.javafullstack.assignment.assignment.service.impl;

import com.javafullstack.assignment.assignment.entity.User;
import com.javafullstack.assignment.assignment.exceptions.UserNotFountException;
import com.javafullstack.assignment.assignment.model.UserModel;
import com.javafullstack.assignment.assignment.repo.UserRepo;
import com.javafullstack.assignment.assignment.service.UserService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserRepo userRepo;

    @Override
    public List<User> getUsers() {
      List<User> users =   userRepo.findAll();
      List<User> sortedUsers =   users.stream().sorted(Comparator.comparing(User::getName)).collect(Collectors.toList());
      return sortedUsers;
    }

    @Override
    public Optional<User> findUserById(Integer id) {
        List<User> users =   userRepo.findAll();
        Optional<User> user =   users.stream().filter(e->e.getId().equals(id)).findAny();
       return user;
    }

    @Override
    public Optional<User> findUserByEmail(User userObj)  {
        List<User> users =   userRepo.findAll();
        Optional<User> user =   users.stream().
                                filter(e->e.getEmail().equals(userObj.getEmail())).
                                filter(e->e.getPassword().equals(userObj.getPassword())).
                                            findAny();
        return user;
    }

    @Override
    public List<UserModel> getUsersByRole(User userObj) {
        List<UserModel> userModels = new ArrayList<>();
        if(userObj.getRole().equalsIgnoreCase("EMPLOYEE"))
        {
            UserModel userModel = new UserModel();
            BeanUtils.copyProperties(userObj,userModel);
            userModels.add(userModel);
        }
        else{
           List<User> users =  getUsers();
           users.forEach(user -> {
               UserModel userModel = new UserModel();
               BeanUtils.copyProperties(user,userModel);
               userModels.add(userModel);
           });
        }
        return userModels;
    }
}
