package com.javafullstack.assignment.assignment.service;

import com.javafullstack.assignment.assignment.entity.User;
import com.javafullstack.assignment.assignment.model.UserModel;

import java.util.List;
import java.util.Optional;

public interface UserService {

    List<User> getUsers();

    Optional<User> findUserById(Integer id);

    Optional<User> findUserByEmail(User userObj);

    List<UserModel> getUsersByRole(User userObj);
}
